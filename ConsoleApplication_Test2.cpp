﻿#include <iostream>
#include <string>
using namespace std;

int main()
{
    cout << "Enter first of two words: ";
    string first;
    cin >> first;
    
    cout << "Enter second of two words: ";
    string second;
    cin >> second;

    string result = first + " " + second;
    cout << result << "\n";
    cout << "Lengh of phrase: " << result.size() << " letters\n";
    cout << "First letter: " << result [0]<<"\n";
    cout << "Last letter: " << result [result.size () - 1] << "\n";

    return 0;
}


